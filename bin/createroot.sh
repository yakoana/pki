#!/bin/bash
openssl req -new -x509 -out caroot.pem -days 36524 -sha256 -text -config caroot.cnf
openssl x509 -in caroot.pem  -out caroot.cer -outform DER

