files=(~/pki/acs/end/certs/*.pem)


for i in $(seq ${#files[@]})
do
	echo $i
	filename=$(basename "${files[i - 1]}")
	directory=$(dirname "${files[i - 1]}")
	extension="${filename##*.}"
	filename="${filename%.*}"

	if [ ! -e "$directory/$filename.cer" ]; then
		echo openssl x509 -in ${files[i - 1]} -out $directory/$filename.cer -outform DER
		openssl x509 -in ${files[i - 1]} -out $directory/$filename.cer -outform DER
	fi
done


