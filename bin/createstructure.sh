#!/bin/bash
thisdir=$(dirname $0)
acdir=$thisdir/../acs
confdir=$thisdir/../conf

rm -rf $acdir/$1
mkdir $acdir/$1
cp $confdir/ca$1.cnf $acdir/$1/ca$1.cnf
cp $confdir/index.txt $acdir/$1/index.txt
cp $confdir/serial $acdir/$1/serial
cp $confdir/crlnumber $acdir/$1/crlnumber
cp $(dirname $0)/create$1.sh $acdir/$1/create$1.sh
mkdir $acdir/$1/certs
mkdir $acdir/$1/crl
mkdir $acdir/$1/newcerts
mkdir $acdir/$1/private
mkdir $acdir/$1/usr


cd $acdir/$1
./create$1.sh $1 $2 
cd $thisdir
