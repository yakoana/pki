#!/bin/bash
current=$(pwd)
	thisdir=$(dirname "$0")
	cd "$thisdir"
	cd ..
	pkdir="$(pwd)"
	echo  $pkdir
cd "$current"

usage="Usage: certificate type(1=PJ, 2=PF, 3=EQ) <file> [request]\n  Where\n\tfile is the base filename\nIt could depend on exported vars.\n the request file is optional."

if [ -z "$1" ] || [ -z "$2" ]; then
	echo -e $usage
	exit 1
fi

echo "usage ok"

certbasename=$(basename -- "$2")
certbasename="${certbasename%.*}"

if [ -f "$2" ]; then
	source $2
else
	echo "Source/request does not exist."
	exit
fi

echo "sources ok"


cert=$pkdir/acs/end/certs/$certbasename

if [ -f $cert.$1.pem ]; then
	echo "$cert.$1.pem file already exist."
	exit
fi

echo "cert open for creation"


if [ "$1" == "1" ]; then
	extfile=$pkdir/conf/requestPJ.cnf
else 
	if [ "$1" == "2" ]; then
		extfile=$pkdir/conf/requestPF.cnf
	else
		if [ "$1" == "3" ]; then
			extfile=$pkdir/conf/requestEQ.cnf
		else
			echo -e $usage
			exit 1
		fi
	fi
fi

echo "cert type ok"


certreq=$cert.$1.req
if [ -z "$3"]; then
	openssl req -batch -newkey rsa:2048 -keyout $cert.$1.key -out $certreq -config $extfile
else
	echo "certreq provided"
	cp $3 $cert.$1.req
fi

echo "cert request validated"


if [ -f $certreq ]; then
	echo "certreq found"
	cd $pkdir/acs/end
	openssl ca -config $pkdir/acs/end/caend.cnf -notext -passin pass:secret -batch -in $certreq -out $cert.$1.pem -days 18263 -extfile $extfile -extensions altv3sign
	cd $current
	echo "$2 done"
else
	echo "certreq not found"
fi

echo "cert signed"


if [ -f $cert.$1.pem ]; then
	openssl crl2pkcs7 -nocrl -certfile $cert.$1.pem -certfile "$pkdir/acs/end/endcert.pem" -certfile "$pkdir/acs/inter/intercert.pem" -certfile "$pkdir/acs/root/caroot.pem" -out $cert.$1.p7b
	echo "crl2pkcs7 ok"
	openssl pkcs7 -in $cert.$1.p7b -out $cert.$1.p7 -print_certs
	echo "pkcs7 ok"
	if [ -f $cert.$1.key ]; then
		openssl pkcs12 -export -in $cert.$1.p7 -inkey $cert.$1.key -passin pass:secret -passout pass:secret -out $cert.$1.p12
	fi
	echo "pkcs12 ok"
	openssl x509 -in $cert.$1.pem -out $cert.$1.cer -outform DER
	echo "x509 ok"
else
	echo "failed to create $cert.$1.pem."
fi


