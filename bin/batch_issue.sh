if [ -n "$1" ] && [ -n "$2" ]; then

	EMISSOR=DIKRJ
	for i in $(seq $1 $2)
	do
		SPONSORID=$(date -d "$((RANDOM%10+1980))-$((RANDOM%12+1))-$((RANDOM%28+1))" '+%d%m%Y')
		for j in $(seq 37)
		do
			SPONSORID=$SPONSORID$((RANDOM%10))
		done
		SPONSORID=$SPONSORID$EMISSOR

		COMPANYCEI=""
		for j in $(seq 12)
		do
			COMPANYCEI=$COMPANYCEI$((RANDOM%10))
		done
		COMPANYID=""
		for j in $(seq 14)
		do
			COMPANYID=$COMPANYID$((RANDOM%10))
		done
		echo "export EMAIL=$i.mail@mail.com"		>  $i.tmp
		echo "export SPONSOR=$i"			>> $i.tmp
		echo "export CN=$i" 				>> $i.tmp
		echo "export SPONSORID=$SPONSORID"		>> $i.tmp
		echo "export COMPANYCEI=$COMPANYCEI"		>> $i.tmp
		echo "export COMPANYID=$COMPANYID"		>> $i.tmp
		echo "export CRL=http://localhost/ac/end.crl"	>> $i.tmp
		echo "export AIA=http://localhost/ac/end.html"	>> $i.tmp

		echo $i
		 ~/pki/bin/certificate.sh 1 $i &> /dev/null
		rm $i.tmp
	done
fi

