if [ -z "$1" ]; then
	echo usage revokeend.sh certtorevoke
	exit
fi


if [ -f $1 ]; then
	current=$(pwd)
	cd ~/pki/acs/end
	openssl ca -config caend.cnf -revoke $1 -key secret
	cd $current
else
	echo "No such $1 file"
fi


