@ECHO OFF
SETLOCAL 

:INIT
SET ME=%~n0
SET __PARAM=%~1
IF "%__PARAM%" NEQ "" ( SET OPENSSL=%__PARAM% )
FOR /F %%i IN ("%~dp0..") DO SET PKI=%%~fi
SET CONF=%PKI%\conf
SET ACS=%PKI%\acs
SET REPO=%PKI%\repository
SET CA_CMD=%~1%\bin\openssl.exe
IF NOT EXIST %CA_CMD% ( SET CA_CMD=%OPENSSL%\bin\openssl.exe )

IF NOT EXIST %CONF%\index.txt   ( GOTO:REQUIREMENT )
IF NOT EXIST %CONF%\serial      ( GOTO:REQUIREMENT )
IF NOT EXIST %CONF%\crlnumber   ( GOTO:REQUIREMENT )
IF NOT EXIST %CONF%\caroot.cnf  ( GOTO:REQUIREMENT )
IF NOT EXIST %CONF%\cainter.cnf ( GOTO:REQUIREMENT )
IF NOT EXIST %CONF%\caend.cnf   ( GOTO:REQUIREMENT )
SET __PARAM=

:OPENSSL_VERSION
IF NOT EXIST %CA_CMD%           ( GOTO:REQUIREMENT )
FOR /F "tokens=* USEBACKQ" %%i IN ( `CALL %CA_CMD% version` ) DO ( SET __SSL_VER=%%i )
SET __VERSION=%__SSL_VER:~8,6%
IF "%__VERSION%" NEQ "1.1.0f"   ( GOTO:REQUIREMENT )
SET __SSL_VER=
SET __VERSION=

:NEW_PKI
SET /P __INPUT=All existing directories will be deleted. Are you sure? y/n: || SET __INPUT=y
IF /I "%__INPUT%" NEQ "Y"       ( GOTO:END )
RMDIR /S /Q "%ACS%"
RMDIR /S /Q "%REPO%"
MKDIR "%REPO%"
CALL:CREATE_STRUCT root
IF %ERRORLEVEL% NEQ 0 ( GOTO:END )
CALL:CREATE_STRUCT inter
IF %ERRORLEVEL% NEQ 0 ( GOTO:END )
CALL:CREATE_STRUCT end
IF %ERRORLEVEL% NEQ 0 ( GOTO:END )
CALL:CREATE_CHAIN
IF %ERRORLEVEL% NEQ 0 ( GOTO:END )
SET __INPUT=
GOTO:END

:CREATE_STRUCT
SET __CADIR=%~1
SET __CAFILE=ca%~1.cnf
MKDIR %ACS%\%__CADIR%\certs
MKDIR %ACS%\%__CADIR%\crl
MKDIR %ACS%\%__CADIR%\newcerts
MKDIR %ACS%\%__CADIR%\private
MKDIR %ACS%\%__CADIR%\usr
COPY %CONF%\%__CAFILE% %ACS%\%__CADIR%\%__CAFILE%
COPY %CONF%\index.txt  %ACS%\%__CADIR%\index.txt
COPY %CONF%\serial     %ACS%\%__CADIR%\serial
COPY %CONF%\crlnumber  %ACS%\%__CADIR%\crlnumber
CALL:CREATE_%__CADIR%
SET __CADIR=
SET __CAFILE=
GOTO:EOF

:CREATE_ROOT
SET ROOTCA_DIR=%ACS%\root
ECHO unique_subject = yes>%ACS%\root\index.txt.attr
CALL %CA_CMD% req -new -x509 -out %ACS%\root\caroot.pem -days 3652 -sha256 -text -config %ACS%\root\caroot.cnf
IF %ERRORLEVEL% NEQ 0 ( GOTO:ERROR )
CALL %CA_CMD% x509 -in %ACS%\root\caroot.pem  -out %ACS%\root\caroot.cer -outform DER
IF %ERRORLEVEL% NEQ 0 ( GOTO:ERROR )
GOTO:EOF

:CREATE_INTER
SET INTERCA_DIR=%ACS%\inter
ECHO unique_subject = yes>%ACS%\inter\index.txt.attr
CALL %CA_CMD% req -new -config %ACS%\inter\cainter.cnf -out %ACS%\root\usr\inter-req.pem -days 3651 -text
IF %ERRORLEVEL% NEQ 0 ( GOTO:ERROR )
CALL %CA_CMD% ca -config %ACS%\root\caroot.cnf -extensions v3_ca_inter -md sha256 -batch -passin pass:secret -in %ACS%\root\usr\inter-req.pem -out %ACS%\inter\intercert.pem -days 3651
IF %ERRORLEVEL% NEQ 0 ( GOTO:ERROR )
CALL %CA_CMD% x509 -in %ACS%\inter\intercert.pem  -out %ACS%\inter\intercert.cer -outform DER
IF %ERRORLEVEL% NEQ 0 ( GOTO:ERROR )
GOTO:EOF

:CREATE_END
SET ENDCA_DIR=%ACS%\end
ECHO unique_subject = no>%ACS%\end\index.txt.attr
CALL %CA_CMD% req -new -config %ACS%\end\caend.cnf -out %ACS%\inter\usr\end-req.pem -days 3650 -text
IF %ERRORLEVEL% NEQ 0 ( GOTO:ERROR )
CALL %CA_CMD% ca -config %ACS%\inter\cainter.cnf -extensions v3_ca_end -md sha256 -batch -passin pass:secret -in %ACS%\inter\usr\end-req.pem -out %ACS%\end\endcert.pem -days 3650
IF %ERRORLEVEL% NEQ 0 ( GOTO:ERROR )
CALL %CA_CMD% x509 -in %ACS%\end\endcert.pem  -out %ACS%\end\endcert.cer -outform DER
IF %ERRORLEVEL% NEQ 0 ( GOTO:ERROR )
GOTO:EOF

:CREATE_CHAIN
CALL %CA_CMD% crl2pkcs7 -nocrl -certfile "%ACS%\end\endcert.pem" -certfile "%ACS%\inter\intercert.pem" -certfile "%ACS%\root\caroot.pem" -out "%REPO%\chain.p7b"
IF %ERRORLEVEL% NEQ 0 ( GOTO:ERROR )
CALL %CA_CMD% pkcs7 -in "%REPO%\chain.p7b" -out "%REPO%\chain.p7" -print_certs
IF %ERRORLEVEL% NEQ 0 ( GOTO:ERROR )
GOTO:EOF

:ERROR
ECHO.
ECHO %ME%: Error %ERRORLEVEL% during execution of OpenSSL
ECHO %ME%: PKI not created
GOTO:END

:REQUIREMENT
ECHO.
ECHO %ME%: OpenSSL root directory must be passed as argument or found at an OPENSSL environment variable
ECHO %ME%: OpenSSL version must be 1.1.0f
ECHO %ME%: Operational files index.txt, serial and crlnumber must exist at %CONF% directory
ECHO %ME%: Configuration files caroot.cnf, cainter.cnf and caend.cnf must exist at %CONF% directory

:END
ENDLOCAL
IF %ERRORLEVEL% NEQ 0 ( PAUSE )