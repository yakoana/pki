#!/bin/bash
echo here we are
OPENSSL=$(which openssl);
echo and gone
if [ -z "$OPENSSL" ]; then
echo Openssl not found. Sorry.
exit
fi

echo Are you sure ? [y/N]
read opt

case $opt in
y|Y|yes|YES|Yes)

	echo "Removing old chain (do something on your browser or you're a dead man)"
	
	rm -f -r ../acs
	rm -f -r ../repository

	echo Creating dirs
	mkdir ../acs
	mkdir ../repository

	echo "Creating Root."
	./createstructure.sh root root
	echo "Creating Intermediate CA."
	./createstructure.sh inter root
	echo "Creating End User CA."
	./createstructure.sh end inter
	;;


*)
	echo "Then you live for another day."
	;;

esac



