1) Emitindo Certificados ICP-Brasil:
	1.1) Crie um arquivo exportando os valores que serão colocados no certificado.
		1.1.1) Equipamento:
			set ALT_NAMES=EQ
			set EMAIL=email do certificado
			set SPONSOR=Nome da pessoa responsavel pelo certificado
			set CN=Nome da empresa
			set SPONSORID=ver explicação
			set COMPANY=Nome da Empresa
			set COMPANYID=CNPJ
			
		1.1.2) PJ
			set ALT_NAMES=PJ
			set EMAIL=email do certificado
			set SPONSOR=Nome da pessoa responsavel pelo certificado
			set CN=Nome da empresa
			set SPONSORID=ver explicação
			set COMPANYCEI=CEI (12 zeros se não tiver)
			set COMPANYID=CNPJ
			
		Explicação do SPONSORID
		OID = 2.16.76.1.3.4 e conteúdo = nas primeiras 8 (oito) posições, a data de nascimento do responsável pelo certificado, no formato ddmmaaaa; nas 11 (onze) posições subseqüentes, o Cadastro de Pessoa Física (CPF) do responsável; nas 11 (onze) posições subseqüentes, o número de inscrição do responsável no PIS/PASEP; nas 11 (onze) posições subseqüentes, o número do RG do responsável; nas 6 (seis) posições subseqüentes, as siglas do órgão expedidor do RG e respectiva UF 
		Exemplo: set SPONSORID=197504020000000000000000000000000000000000000DIKRJ
		
		Algumas restrições:
			- O arquivo deve estar no diretório bin e ter extensão tmp
			- O arquivo deve ter um nome único, pois o certificado será criado com este mesmo nome, sem a extensão
	1.2) No diretório bin execute: 
		Certificado PJ
			certificate.bat 1 <nome do arquivo sem o tmp>
		Certificado Equipamento
			certificate.bat 3 <nome do arquivo sem o tmp>
			
		Serão criados no diretório acs\end\certs:
			1) <nome do arquivo sem o tmp>.pem - Arquivo com o X.509 do certificado em PEM (Base 64 + Cert Armor)
			2) <nome do arquivo sem o tmp>.cer - Arquivo com o X.509 do certificado em DER (Binário ASN.1)
			3) <nome do arquivo sem o tmp>.key - Arquivo com o a chave privada do certificado em PEM
			4) <nome do arquivo sem o tmp>.p12 - Arquivo com o Envelope PKCS#12 contendo chaves e cadeia de certificados.
			
2) Revogando um certificado:
	2.1) Usando o nome do arquivo(sem o .tmp) que foi usado para gerar o certificado, execute no diretório bin:
		revoke.bat <nome do arquivo sem o tmp>
		
		Nota 1: o arquivo .tmp não precisa mais existir, porém o arquivo .pem gerado precisa ainda estar em acs\end\certs
		Nota 2: Há maneiras de recuperar o arquivo .pem se tiver o arquivo .p12 ou o arquivo.cer criado. use a openssl que vai no diretório bin. 
		Sugestão 1: Não apague o arquivo .pem gerado.
		Nota 3: Há maneiras de revogar mesmo sem o .pem. Editando o index.txt.
		Sugestão 2: Faça backup antes.
		Sugestão 3: Não edite na mão se não souber o que está fazendo.
		Sugestão 4: Leia de novo a sugestão 1.
		
		
		
3) Emitindo as LCRs
	3.1) no diretório bin executar o arquivo updatecrl.bat. Serão geradas 3 lcrs:
		3.1.1)	endcrl.crl
		3.1.2)	intercrl.crl
		3.1.3)	rootcrl.crl
		

		
			


			
			
			
			
			


