current=$(pwd)
data=$(date +%Y_%m_%d-%H_%M)

cd ~/pki/acs/end
crlnumber=$(cat crlnumber)
if [ -z "$crlnumber" ]; then
	if [ -f crlnumber.old ]; then
		cp crlnumber.old crlnumber

	else
		echo "00" > crlnumber
	fi
fi
openssl ca -config caend.cnf -gencrl -out crl/crl.$data.pem -key secret -crlhours 1
openssl crl -in crl/crl.$data.pem -out crl/crl.crl -outform DER
cp crl/crl.crl ../../repository/endcrl.tmp
mv ../../repository/endcrl.tmp ../../repository/endcrl.crl



cd ../inter
crlnumber=$(cat crlnumber)
if [ -z "$crlnumber" ]; then
	if [ -f crlnumber.old ]; then
		cp crlnumber.old crlnumber

	else
		echo 00 > crlnumber
	fi
fi
openssl ca -config cainter.cnf -gencrl -out crl/crl.$data.pem -key secret -crlhours 1
openssl crl -in crl/crl.$data.pem -out crl/crl.crl -outform DER
cp crl/crl.crl ../../repository/intercrl.tmp
mv ../../repository/intercrl.tmp ../../repository/intercrl.crl




cd ../root
crlnumber=$(cat crlnumber)
if [ -z "$crlnumber" ]; then
	if [ -f crlnumber.old ]; then
		cp crlnumber.old crlnumber

	else
		echo 00 > crlnumber
	fi
fi
openssl ca -config caroot.cnf -gencrl -out crl/crl.$data.pem -key secret -crlhours 1
openssl crl -in crl/crl.$data.pem -out crl/crl.crl -outform DER
cp crl/crl.crl ../../repository/rootcrl.tmp
mv ../../repository/rootcrl.tmp ../../repository/rootcrl.crl



cd $current
