@ECHO OFF
SETLOCAL EnableExtensions
SETLOCAL EnableDelayedExpansion
:: * * * * * * * * * * * * * * * * * * * * * *
:: Requires dev-env.bat:
::    OPENSSL variable must be defined
::    Requires version 1.1.0f
:: Argument (optional)
::    Complete path to request to be signed
::    If this argument is not set, a default
::    one is created (see conf/sign.cfn file)
::    In this case, you must set CN variable
:: * * * * * * * * * * * * * * * * * * * * * *

:CMD_LINE
SET ME=%~n0
SET CUR=%CD%
SET REQUEST=%~f1
SET OUT_FILE=%~p1%~n1
SET __OPENSSL=%~2
FOR %%i IN ("%~dp0..") DO SET "__PKI=%%~fi"
IF "%__OPENSSL%" EQU "" ( SET __OPENSSL=%OPENSSL% )

SET VALIDITY=18263
SET CA_CMD=%__OPENSSL%\bin\openssl.exe
SET SIGN_CONF=%__PKI%\conf\sign.cnf
::SET SIGN_CONF=%__PKI%\conf\server.cnf
SET ENDCA_DIR=%__PKI%\acs\end
SET CA_CONF=%ENDCA_DIR%\caend.cnf
SET ENDCA_CERT=%ENDCA_DIR%\endcert.pem
SET INTERCA_CERT=%__PKI%\acs\inter\intercert.pem
SET ROOTCA_CERT=%__PKI%\acs\root\caroot.pem

:CHECK_ENV
IF     EXIST %OUT_FILE%.pem ( GOTO:USAGE  )
IF NOT EXIST %SIGN_CONF%    ( GOTO:USAGE  )
IF NOT EXIST %CA_CONF%      ( GOTO:USAGE  )
IF NOT EXIST %ENDCA_CERT%   ( GOTO:USAGE  )
IF NOT EXIST %INTERCA_CERT% ( GOTO:USAGE  )
IF NOT EXIST %ROOTCA_CERT%  ( GOTO:USAGE  )
IF NOT EXIST %CA_CMD%       ( GOTO:USAGE  )
IF NOT EXIST %REQUEST%      ( CALL:GENCSR )

:EXECUTE
CALL %CA_CMD% ca -config "%CA_CONF%" -notext -passin pass:secret -batch -in "%REQUEST%" -out "%OUT_FILE%.pem" -days %VALIDITY% -extfile "%SIGN_CONF%" -extensions altv3sign
IF %ERRORLEVEL% NEQ 0 (
	ECHO %ME%: Could not sign certificate request. Please check if OpenSSL is properly installed.
	EXIT /B %ERRORLEVEL%
)
CALL %CA_CMD% crl2pkcs7 -nocrl -certfile "%OUT_FILE%.pem"  -certfile "%ENDCA_CERT%" -certfile "%INTERCA_CERT%" -certfile "%ROOTCA_CERT%" -out "%OUT_FILE%.p7b"
IF %ERRORLEVEL% NEQ 0 (
	ECHO %ME%: Could not generate PKCS #7. Please check if OpenSSL is properly installed.
	EXIT /B %ERRORLEVEL%
)
ECHO.
ECHO Certificate request successfully signed
GOTO:END

:GENCSR
SET KEY_FILE=%OUT_FILE%.key
CALL %CA_CMD% req -batch -newkey rsa:2048 -keyout "%KEY_FILE%" -out "%REQUEST% " -config "%SIGN_CONF%"
IF %ERRORLEVEL% NEQ 0 (
	ECHO %ME%: Could not generate certificate sign request. Please check if OpenSSL is properly installed.
	EXIT /B %ERRORLEVEL%
)
GOTO:EOF

:USAGE
ECHO.
ECHO * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
ECHO Usage: %ME% [request], where
ECHO   request: complete path to certificate signing request. Mandatory. This request
ECHO   should not be reused. Request file must not have extension .pem, .cer, .der,
ECHO   .p7 or .p7b. Sugested: .req
ECHO * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
ECHO.
EXIT /B 1

:END
SET ME=
SET CUR=
SET __PKI=
SET REQUEST=
SET OUT_FILE=
SET VALIDITY=
SET CA_CMD=
SET SIGN_CONF=
SET ENDCA_DIR=
SET CA_CONF=
SET ENDCA_CERT=
SET INTERCA_CERT=
SET ROOTCA_CERT=
SET KEY_FILE=
ENDLOCAL