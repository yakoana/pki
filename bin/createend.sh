#!/bin/bash
echo "unique_subject = no" > index.txt.attr
openssl req -new -config ca$1.cnf -out ../$2/usr/$1-req.pem -days 36522 -text
cd ../$2
openssl ca -config ca$2.cnf -extensions v3_ca_$1 -md sha256 -batch -passin pass:secret -in usr/$1-req.pem -out ../$1/$1cert.pem -days 36522
cd ../$1
openssl x509 -in endcert.pem  -out endcert.cer -outform DER

