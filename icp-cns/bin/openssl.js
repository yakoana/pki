'use strict';

const path = require('path');
const fs = require('fs');
const cp = require('child_process');

const OPENSSL_ERROR = 'A execução da OpenSSL falhou com o erro ';

function generateUUID() { // Public Domain/MIT
    var d = new Date().getTime();//Timestamp
    var d2 = ((typeof performance !== 'undefined') && performance.now && (performance.now()*1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;//random number between 0 and 16
        if(d > 0){//Use timestamp until depleted
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {//Use microseconds since page-load if supported
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}
class GeneratedFiles {
	constructor(key, request) {
		this.key = key;
		this.request = request;
	}
}
class OpenSSLWrapper {
	constructor(createPKI) {
		this.openSSL = path.resolve(__dirname, 'openssl.exe');
		this.pki = path.resolve(__dirname, '..');
		if (createPKI) return;

		let conf = path.resolve(__dirname, '..', 'conf');
		let acs = path.resolve(this.pki, 'acs');
		let configFile = path.resolve(acs, 'issue-conf.json');
		let configJSON = fs.readFileSync(configFile, { encoding: 'utf-8' });
		this.config = JSON.parse(configJSON);
		this.endCaFolder = path.resolve(acs, 'end');
		this.usrFolder = path.resolve(this.endCaFolder, 'usr');
		this.caConf = path.resolve(conf, 'ca.cnf');
		this.signConf = path.resolve(conf, 'sign.cnf');
	}
	execOpenSSL(args, options) {
		let ret = cp.spawnSync(this.openSSL, args, options);
		if (ret.stdout) console.log(new TextDecoder().decode(ret.stdout));
		if (ret.stderr) console.error(new TextDecoder().decode(ret.stderr));
		return ret.status;
	}
	generateCSR(cnf) {
		let options = {
			cwd: this.pki,
			windowsHide: false,
			env: {
				USERPROFILE: process.env.USERPROFILE,
				CERT_TYPE: cnf.tipo,
				SUBJECT_CN: cnf.cn,
				C: this.config.issuer.c,
				O: this.config.issuer.o,
				OU: this.config.issuer.ou,
				CN: this.config.issuer.cn
			}
		};
		let rnd = generateUUID();
		let keyFile = path.resolve(this.usrFolder, rnd + '.key');
		let reqFile = path.resolve(this.usrFolder, rnd + '.req');
		let args = [
			'req',
			'-batch',
			'-newkey',
			'rsa:2048',
			'-keyout',
			keyFile,
			'-out',
			reqFile,
			'-config',
			this.signConf
		];
		let status = this.execOpenSSL(args, options);
		if (status != 0) throw new Error(OPENSSL_ERROR + status.toString());
		return new GeneratedFiles(keyFile, reqFile);
	}
	signCert(cnf, request) {
		let input = path.parse(request);
		let out = path.format({
			dir: input.dir,
			name: input.name,
			ext: '.pem'
		});
		let subjectId = cnf.titular ? cnf.titular.nascimento + cnf.titular.cpf + cnf.titular.nis + cnf.titular.rg + cnf.titular.expedidor : undefined;
		let subjectTE = cnf.tEleitor ? cnf.tEleitor.inscricao + cnf.tEleitor.zona + cnf.tEleitor.secao + cnf.tEleitor.local : undefined;
		let subjectCEI = cnf.cei;
		let sponsorId = cnf.responsavel ? cnf.responsavel. nascimento + cnf.responsavel.cpf + cnf.responsavel.nis + cnf.responsavel.rg + cnf.responsavel.expedidor : undefined;
		let sponsor = cnf.nome;
		let companyId = cnf.cnpj
		let companyCEI = cnf.cei;
		let options = {
			cwd: this.endCaFolder,
			windowsHide: false,
			env: {
				USERPROFILE: process.env.USERPROFILE,
				CERT_TYPE: cnf.tipo,
				SUBJECT_CN: cnf.cn,
				SUBJECTID: subjectId,
				SUBJECTTE: subjectTE,
				SUBJECTCEI: subjectCEI,
				SPONSORID: sponsorId,
				SPONSOR: sponsor,
				COMPANYID: companyId,
				COMPANYCEI: companyCEI,
				C: this.config.issuer.c,
				O: this.config.issuer.o,
				OU: this.config.issuer.ou,
				CN: this.config.issuer.cn
			}
		};
		let pass = 'pass:' + this.config.password;
		let args = [
			'ca',
			'-config',
			this.caConf,
			'-notext',
			'-passin',
			pass,
			'-batch',
			'-in',
			request,
			'-out',
			out,
			'-days',
			this.config.validity.toString(),
			'-extfile',
			this.signConf,
			'-extensions',
			'altv3sign'
		];
		let status = this.execOpenSSL(args, options);
		if (status != 0) throw new Error(OPENSSL_ERROR + status.toString());
		return out;
	}
	createPFX(keyFile, certFile, friendlyName) {
		let input = path.parse(keyFile);
		let out = path.format({
			dir: input.dir,
			name: input.name,
			ext: '.pfx'
		});
		let pass = 'pass:' + this.config.password;
		let args = [
			'pkcs12',
			'-export',
			'-out',
			out,
			'-passout',
			pass,
			'-in',
			certFile,
			'-name',
			friendlyName,
			'-inkey',
			keyFile,
			'-passin',
			pass,
			'-chain',
			'-CAfile',
			this.config.chain
		];
		let status = this.execOpenSSL(args, { cwd: this.pki, windowsHide: false });
		if (status != 0) throw new Error(OPENSSL_ERROR + status.toString());
		return out;
	}
	toDER(certPEM, certDER, options) {
		let args = [
			'x509',
			'-in',
			certPEM,
			'-out',
			certDER,
			'-outform',
			'DER'
		];
		let status = this.execOpenSSL(args, options);
		if (status != 0) throw new Error(OPENSSL_ERROR + status.toString());
		return certDER;
	}
	generateRootCertificate(cnf, acFolder, confFolder, password) {
		let certPEM = path.resolve(acFolder, 'ca.pem');
		let input = path.parse(certPEM);
		let certDER = path.format({
			dir: input.dir,
			name: input.name,
			ext: '.cer'
		});
		let conf = path.resolve(confFolder, 'ca.cnf');
		let options = {
			cwd: this.pki,
			windowsHide: false,
			env: {
				USERPROFILE: process.env.USERPROFILE,
				THIS_CA: acFolder,
				C: cnf.dn.c,
				O: cnf.dn.o,
				OU: cnf.dn.ou,
				CN: cnf.dn.cn.root
			}
		};
		let pass = 'pass:' + password;
		let args = [
			'req',
			'-new',
			'-x509',
			'-out',
			certPEM,
			'-days',
			(cnf.validity + 3).toString(),
			'-sha256',
			'-text',
			'-config',
			conf,
			'-extensions',
			'v3_rootca'
		];
		let status = this.execOpenSSL(args, options);
		if (status != 0) throw new Error(OPENSSL_ERROR + status.toString());
		this.toDER(certPEM, certDER, options);
		return certPEM;
	}
	generateCACertificate(cnf, acFolder, issuerFolder, confFolder, leaf, password) {
		let request = path.resolve(issuerFolder, 'usr', 'request.pem');
		let certPEM = path.resolve(acFolder, 'ca.pem');
		let input = path.parse(certPEM);
		let certDER = path.format({
			dir: input.dir,
			name: input.name,
			ext: '.cer'
		});
		let conf = path.resolve(confFolder, 'ca.cnf');
		let options = {
			cwd: this.pki,
			windowsHide: false,
			env: {
				USERPROFILE: process.env.USERPROFILE,
				THIS_CA: acFolder,
				C: cnf.dn.c,
				O: cnf.dn.o,
				OU: cnf.dn.ou,
				CN: leaf ? cnf.dn.cn.end : cnf.dn.cn.inter
			}
		};
		let validity = (cnf.validity + leaf ? 1 : 2).toString();
		let args = [
			'req',
			'-new',
			'-config',
			conf,
			'-out',
			request,
			'-days',
			validity,
			'-text'
		];
		let status = this.execOpenSSL(args, options);
		if (status != 0) throw new Error(OPENSSL_ERROR + status.toString());
		let ext = leaf ? 'v3_ca' : 'v3_ca_inter';
		let pass = 'pass:' + password;
		options.env.THIS_CA = issuerFolder;
		args = [
			'ca',
			'-config',
			conf,
			'-extensions',
			ext,
			'-md',
			'sha256',
			'-batch',
			'-passin',
			pass,
			'-in',
			request,
			'-out',
			certPEM,
			'-days',
			validity
		];
		status = this.execOpenSSL(args, options);
		if (status != 0) throw new Error(OPENSSL_ERROR + status.toString());
		this.toDER(certPEM, certDER, options);
		return certPEM;
	}
	generateChain(end, inter, root, targetFolder) {
		let pkcs = path.resolve(targetFolder, 'chain.p7b');
		let target = path.resolve(targetFolder, 'chain.p7');
		let args = [
			'crl2pkcs7',
			'-nocrl',
			'-certfile',
			end,
			'-certfile',
			inter,
			'-certfile',
			root,
			'-out',
			pkcs
		];
		let options = { cwd: this.pki, windowsHide: false };
		let status = this.execOpenSSL(args, options);
		if (status != 0) throw new Error(OPENSSL_ERROR + status.toString());
		args = [
			'pkcs7',
			'-in',
			pkcs,
			'-out',
			target,
			'-print_certs'
		];
		status = this.execOpenSSL(args, options);
		if (status != 0) throw new Error(OPENSSL_ERROR + status.toString());
		return target;
	}
	generateCRL(cnf, issuer, validity, issuerFolder, confFolder, password) {
		let acFolder = path.resolve(issuerFolder);
		let out = path.resolve(acFolder, 'crl', 'crl.pem');
		let pass = 'pass:' + password;
		let conf = path.resolve(confFolder, 'ca.cnf');
		let options = {
			cwd: this.pki,
			windowsHide: false,
			env: {
				USERPROFILE: process.env.USERPROFILE,
				THIS_CA: acFolder,
				C: cnf.dn.c,
				O: cnf.dn.o,
				OU: cnf.dn.ou,
				CN: issuer
			}
		};
		let args = [
			'ca',
			'-gencrl',
			'-crldays',
			validity.toString(),
			'-passin',
			pass,
			'-config',
			conf,
			'-out',
			out
		];
		let status = this.execOpenSSL(args, options);
		if (status != 0) throw new Error(OPENSSL_ERROR + status.toString());
		return out;
	}
	publishCRL(cnf, issuer, issuerFolder, source, target) {
		let options = {
			cwd: this.pki,
			windowsHide: false,
			env: {
				USERPROFILE: process.env.USERPROFILE,
				THIS_CA: issuerFolder,
				C: cnf.dn.c,
				O: cnf.dn.o,
				OU: cnf.dn.ou,
				CN: issuer
			}
		};
		let args = [
			'crl',
			'-in',
			source,
			'-out',
			target,
			'-outform',
			'DER'
		];
		let status = this.execOpenSSL(args, options);
		if (status != 0) throw new Error(OPENSSL_ERROR + status.toString());
	}
}

module.exports = {
	OpenSSLWrapper: OpenSSLWrapper
}
