'use strict';

const yargs = require('yargs');
const argv = yargs(process.argv).argv;
const path = require('path');
const fs = require('fs');
const { OpenSSLWrapper } = require('./openssl');

const FILE_NOT_FOUND = 'O arquivo %s, referido no argumento, não foi encontrado.';
const LOAD_ERROR = 'Ocorreu o seguinte erro ao carregar o arquivo %s: %s';
const CLEANUP_ERROR = 'Ocorreu o seguinte erro ao remover os arquivos temporários: %s';
const ISSUED_CERT = 'Foram emitidos os seguintes arquivos PKCS #12:';

function sprintf() {
	let output = '';
	if (arguments.length > 0) {
		let replacer = [];
		for (let i = arguments.length - 1; i > 0; i--) replacer.push(arguments[i]);
		let input = arguments[0];
		output = input.replaceAll('%s', () => { return replacer.pop(); });
	}
	return output;
}

function removeAll(targetPath) {
	if (!fs.existsSync(targetPath)) return;
	let files = fs.readdirSync(targetPath);
	files.forEach((filename) => {
		let target = path.resolve(targetPath, filename);
		if (fs.statSync(target).isDirectory()) removeAll(target);
		else fs.unlinkSync(target);
	});
	fs.rmdirSync(targetPath);
}
function loadTextFile(file) {
	try {
		let target = path.resolve(file);
		if (!fs.existsSync(target)) throw new Error(sprintf(FILE_NOT_FOUND, file));
		return fs.readFileSync(target, { encoding: 'utf-8'});
	}
	catch (e) { throw new Error(sprintf(LOAD_ERROR, file, e.message)); }
}
function createCAFolder(sourcePath, targetPath, uniqueSubject) {
	let targetFolders = [
		path.resolve(targetPath, 'certs'),
		path.resolve(targetPath, 'crl'),
		path.resolve(targetPath, 'newcerts'),
		path.resolve(targetPath, 'private'),
		path.resolve(targetPath, 'usr')
	];
	let copy = new Map();
	copy.set( path.resolve(sourcePath, 'index.txt'), path.resolve(targetPath, 'index.txt'));
	copy.set( path.resolve(sourcePath, 'serial'), path.resolve(targetPath, 'serial'));
	copy.set( path.resolve(sourcePath, 'crlnumber'), path.resolve(targetPath, 'crlnumber'));
	let i = 0;
	while (i < targetFolders.length) fs.mkdirSync(targetFolders[i++], { recursive: true });
	let it = copy.entries();
	let next;
	while ((next = it.next().value)) fs.copyFileSync(next[0], next[1]);
	let attr = uniqueSubject ? 'unique_subject = yes' : 'unique_subject = no';
	fs.writeFileSync(path.resolve(targetPath, 'index.txt.attr'), attr);
}
function createPKI(config, password) {
	let input = loadTextFile(config);
	let cnf = JSON.parse(input);
	let confFolder = path.resolve(__dirname, '..', 'conf');
	let acsFolder = path.resolve(__dirname, '..', 'acs');
	let rootFolder = path.resolve(acsFolder, 'root');
	let interFolder = path.resolve(acsFolder, 'inter');
	let endFolder = path.resolve(acsFolder, 'end');
	let repositoryFolder = path.resolve(acsFolder, 'repository');

	removeAll(acsFolder);
	let wrapper = new OpenSSLWrapper(true);
	
	createCAFolder(confFolder, rootFolder, true);
	fs.mkdirSync(repositoryFolder);
	let root = wrapper.generateRootCertificate(cnf, rootFolder, confFolder);
	let crl = wrapper.generateCRL(cnf, cnf.dn.cn.root, cnf.validity + 3, rootFolder, confFolder, password);
	wrapper.publishCRL(cnf, cnf.dn.cn.root, rootFolder, crl, path.resolve(repositoryFolder, 'root.crl'));
	
	createCAFolder(confFolder, interFolder, true);
	let inter = wrapper.generateCACertificate(cnf, interFolder, rootFolder, confFolder, false, password);
	crl = wrapper.generateCRL(cnf, cnf.dn.cn.inter, cnf.validity + 2, interFolder, confFolder, password);
	wrapper.publishCRL(cnf, cnf.dn.cn.inter, interFolder, crl, path.resolve(repositoryFolder, 'inter.crl'));
	
	createCAFolder(confFolder, path.resolve(acsFolder, 'end'), false);
	let end = wrapper.generateCACertificate(cnf, endFolder, interFolder, confFolder, true, password);
	crl = wrapper.generateCRL(cnf, cnf.dn.cn.end, cnf.validity + 1, endFolder, confFolder, password);
	wrapper.publishCRL(cnf, cnf.dn.cn.end, endFolder, crl, path.resolve(repositoryFolder, 'end.crl'));
	
	let chain = wrapper.generateChain(end, inter, root, repositoryFolder);
	let iConfig = {
		password: password,
		validity: cnf.validity,
		issuer: {
			c: cnf.dn.c,
			o: cnf.dn.o,
			ou: cnf.dn.ou,
			cn: cnf.dn.cn.end
		},
		chain: chain
	}
	let iConfigFile = path.resolve(acsFolder, 'issue-conf.json');
	fs.writeFileSync(iConfigFile, JSON.stringify(iConfig));
}

function cleanUp(keyFile, request, certFile) {
	try {
		fs.unlinkSync(keyFile);
		fs.unlinkSync(request);
		fs.unlinkSync(certFile);
	}
	catch (e) { console.error(sprintf(CLEANUP_ERROR, e)); }
}
function issue(cnf) {
	let wrapper = new OpenSSLWrapper();
	let ret = wrapper.generateCSR(cnf);
	let cert = wrapper.signCert(cnf, ret.request);
	let pfx = wrapper.createPFX(ret.key, cert, cnf.cn);
	cleanUp(ret.key, ret.request, cert);
	return pfx;
}


(function () {

	if (!argv.config) throw new Error('Argumento --config=[json] obrigatório');
	if (argv.issue) {
		let input = loadTextFile(argv.config);
		let cnf = JSON.parse(input);
		let i = 0;
		let result = [];
		while (i < cnf.length) {
			let pfx = issue(cnf[i++]);
			result.push(pfx);
		}
		console.log(ISSUED_CERT);
		result.forEach((value) => { console.log(value); });
	}
	else if (argv.create) createPKI(argv.config, argv.password);

}());
